package main

import (
	"bytes"
	"fmt"
	"strings"
)

type Node struct {
	id       string
	vertices []Node
}

type Graph struct {
	nodes map[string]*Node
}

func (n Node) String() string {
	if n.vertices == nil {
		return n.id
	}
	b := new(bytes.Buffer)
	fmt.Fprintf(b, "id:%s, vertices: ", n.id)
	for _, value := range n.vertices {
		fmt.Fprintf(b, "%s,", value.id)
	}
	return b.String()
}

func (g Graph) String() string {
	if g.nodes == nil {
		return ""
	}
	b := new(bytes.Buffer)
	for key, value := range g.nodes {
		fmt.Fprintf(b, "%s=%s\n", key, value)
	}
	return b.String()
}

/*
* CreateGraph
* nodes: {"1", "2", "3", "4", "5", "6", "7", "8"}
* connections: {"1-2", "1-3", "1-4", "2-5", "3-6", "4-7", "5-8", "6-8", "7-8"}
 */
func CreateGraph(nodes []string, connections []string) *Graph {
	graph := Graph{}
	graph.nodes = make(map[string]*Node)
	for _, nodeId := range nodes {
		node := Node{nodeId, make([]Node, 0)}
		graph.nodes[nodeId] = &node
	}
	for _, connection := range connections {
		edgeElements := strings.Split(connection, "-")
		if len(edgeElements) != 2 {
			panic("Invalid connections array")
		}
		from := graph.nodes[edgeElements[0]]
		to := graph.nodes[edgeElements[1]]
		AppendVertice(from, to)
	}
	return &graph
}

func AppendVertice(node, nodeToAdd *Node) {
	verts := node.vertices
	node.vertices = append(verts, *nodeToAdd)

}

/*
* Traverse over graph, return Node ids in slice of traversing order
 */
func TraverseBreadFirst(graph Graph, entryNodeId string) []string {
	// nodesLength := len(graph.nodes)
	traverseHistory := make([]string, 0)
	visitedNodes := make(map[string]string)
	queue := []Node{*graph.nodes[entryNodeId]}
	for len(queue) != 0 {
		first := queue[:1][0]
		queue = queue[1:]
		_, alreadyVisited := visitedNodes[first.id]
		if alreadyVisited {
			continue
		}
		visitedNodes[first.id] = first.id
		traverseHistory = append(traverseHistory, first.id)
		for _, v := range first.vertices {
			queue = append(queue, *graph.nodes[v.id])
		}
	}
	return traverseHistory
}

/*
* Traverse over graph, return Node ids in slice of traversing order
 */
func TraverseDeepFirst(graph Graph, entryNodeId string) []string {
	traverseHistory := make([]string, 0)
	visitedNodes := make(map[string]string)
	queue := []Node{*graph.nodes[entryNodeId]}
	for len(queue) != 0 {
		first := queue[:1][0]
		queue = queue[1:]
		_, alreadyVisited := visitedNodes[first.id]
		if alreadyVisited {
			continue
		}
		traverseHistory = append(traverseHistory, first.id)
		visitedNodes[first.id] = first.id
		for _, v := range first.vertices {
			firstQueue := []Node{*graph.nodes[v.id]}
			queue = append(firstQueue, queue...)
		}
	}
	return traverseHistory
}

func main() {
	nodes := []string{"1", "2", "3", "4", "5", "6", "7", "8"}
	connections := []string{"1-2", "1-3", "1-4", "2-5", "3-6", "4-7", "5-8", "6-8", "7-8"}
	graph := CreateGraph(nodes, connections)
	fmt.Print(graph)
	fmt.Println(TraverseBreadFirst(*graph, "1"))
	fmt.Println(TraverseDeepFirst(*graph, "1"))
}
