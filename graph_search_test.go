package main

import (
	"reflect"
	"testing"
)

func TestTraverseBreadFirst(t *testing.T) {
	nodes := []string{"1", "2", "3", "4", "5", "6", "7", "8"}
	connections := []string{"1-2", "1-3", "1-4", "2-5", "3-6", "4-7", "5-8", "6-8", "7-8"}
	graph := CreateGraph(nodes, connections)
	traverse := TraverseBreadFirst(*graph, "1")
	if !reflect.DeepEqual(traverse, []string{"1", "2", "3", "4", "5", "6", "7", "8"}) {
		panic("")
	}
}


func TestTraverseDeepFirst(t *testing.T) {
	nodes := []string{"1", "2", "3", "4", "5", "6", "7", "8"}
	connections := []string{"1-2", "1-3", "1-4", "2-5", "3-6", "4-7", "5-8", "6-8", "7-8"}
	graph := CreateGraph(nodes, connections)
	traverse := TraverseDeepFirst(*graph, "1")
	if !reflect.DeepEqual(traverse, []string{"1", "4", "7", "8", "3", "6", "2", "5"}) {
		panic("")
	}
}